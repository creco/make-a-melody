// A verry easy Synth in Supercollider to be Controlled by OSC
// As A Example of the Iannix graphical Sequencer Communicated by OSC

// Open the Right OSC Channel
// Tip: This working only when in Menue::Server the Option Server Dump OSC is activated
n = NetAddr("127.0.0.1", 1234); // local machine

// Controlling is Connection ready. Listening of Key /cursor
OSCdef.newMatching(\MeasRhoProcessingMan, { |msg, time, addr, recvPort| msg.postln; }, '/cursor', n);

// Easy to controlled Synth
// Has Amplitude and Frequency Modulation
(
SynthDef(\synthy, { |freq1=440, freq2=440, // Basic Frequency for right and left Chanell seperated
	amp1=1, amp2=1, // Basic Amplitude for right and left Chanell seperated
	ampFreq1=1, ampFreq2=1, // Basic Amp for Amplitude Modulation for right and left Chanell seperated
	ampamp1=0, ampamp2=0,   // Basic Freq for Amplitude Modulation for right and left Chanell seperated
	freqFreq=1, freqamp2=0| // Freq and Amp for Frequency Modulation both Channels equal
	var test, freqq1, freqq2;
	test = SinOsc.kr(freqFreq, 0, freqamp2/2); // LFO for Frequency Modulation
	// Frequency Modulation to both Channels seperatly
	freqq1 = freq1 + (freq1*test);
	freqq2 = freq2 + (freq2*test);
	// Oscillators for Both Channels seperatly with Amplitude Modulation by Multiplicator mapped to standard Output
	Out.ar(0, SinOsc.ar(freqq1, 0, amp2*SinOsc.kr(ampFreq1, 0, ampamp2))+SinOsc.ar(freqq2, 0, amp2*SinOsc.kr(ampFreq2,0, ampamp2)));
}).add;
)

// Activate Synth for running in Standby
a = Synth(\synthy);

// Function to Process incoming OSC Messages from receiver Function
( ~processingMessage = { arg message;
// Input of Message Array
// Case if to understand kind of Message
// Controlled by First Paramter of Message Array
// Then give other Parameters to right Control of Synth
// Explained at first Case
	if (message[1] == 6.0) // here Kind of Message is 6 by Parameter
	{
		a.set(\freq1, message[2]*880); // So give next Parameter to left Chanel ground Frequency
		a.set(\amp1, message[3]);      // So give even next Parameter to left Chanel Amplitude.
	};
	if (message[1] == 10.0)
	{
		a.set(\freq2, message[2]*880);
		a.set(\amp2, message[3]);
	};
	if (message[1] == 6.0)
	{
		a.set(\ampFreq1, message[2]*18);
		a.set(\ampamp1, message[3]);
	};
	if (message[1] == 10.0)
	{
		a.set(\ampFreq2, message[2]*18);
		a.set(\ampamp2, message[3]);
	};/**/
	if (message[1] == 10.0)
	{
		a.set(\freqFreq, message[2]*18);
		a.set(\freqamp2, message[3]);
	};
});

// Chatch Incomming OSC Messages on by Paramter n specific Address and Port.
// And Message Tag /cursor
// Calls Function to process the Message see above
OSCdef.newMatching(\I, { |msg, time, addr, recvPort| ~processingMessage.value(msg); }, '/cursor', n);
