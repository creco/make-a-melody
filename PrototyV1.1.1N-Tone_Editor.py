#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Import all Needed foreign Classes out of Library """
# Import of tkinter Modules
# Frames and MessageBoxes imported from tkinter
from tkinter import Tk, Frame, filedialog, messagebox
# Widgets imported from tkinter
from tkinter import PanedWindow, Button, Entry, Menu
# Symbols imported from tkinter
from tkinter import VERTICAL, BOTH
# Command Control Dialogs imported from tkinterger
from tkinter.simpledialog import askinteger

# import Numpy
import numpy
import numpy as np
from numpy import array


# import of webbrowser for showing Documentation.html
import webbrowser as web

"""
Created on Mon Apr 27 10:38:33 2020

@author: thomas """


"""
N-Tone Serialism == Tollkit

A simple first Working Version of a
N-Tone Editor
for more Informations see:
    https://git.forum.ircam.fr/creco/make-a-melody/-/wikis/Serialism-Toll-Kit

Author: Thomas Lahme
Last modified: April 2020
Work in progressive"""


class CalcTabel:
    """ Class to Process N-Tone Tables
        This Class is Public
    """
    
    # Debuging Definitions should been Removed
    dx = 3 # Preset for the Rows of a Table should be next removed
    dy = 4 # Preset for the Columns of a Table should be next removed
    # A empty Table of the Preset Size
    master_table = numpy.zeros(shape=(dx, dy)) 
    
    def set_delta(self, x):
        """ Function to set the correct Size
            x = Size of a N*N Matrix / Table
            This Function is Public
        """
        
        # Attentio: We define x and y Dimension of the Matrix seperatly
        # This is needed in Relation to handle improper N-Tone Tables
        self.dx = x # Size of the Row Dimension of the Matrix
        self.dy = x # Size of the Column Dimension of the Matrix
        self.master_table = numpy.zeros(shape=(self.dx, self.dy))

    def transpond(self, table):
        """ Read the N-Tone Table in the customized Table Widget
            table = The Table Widget to write it
            This Function is Public
        """
        for i in range(self.dx):  # Iterrating over Rows
            for j in range(self.dy):  # Iterating over Columns
                temp = str(self.master_table[i][j])
                # Process of Interchange
                table.write_cell(rows=i, column=j, value=temp)

    def construct_vector(self):
        """ Construct a Vector for the Table Generator 
            here aka Construct_taböle
            Returns this Vector
            This Function is private
        """
        
        # Randoms a Whole Vector of needed Size
        temp = np.random.randint(0, 100, self.dx)
        summ = np.sum(temp) # Sums up the whole Vector
        tempres = temp // (summ/100) # Integer Division to re-Value the Vector
        return tempres # returns the ready Vector

    def construct_table(self):
        """ Generate a new filled N-Tone Table
            This Table is garanty to succed the Definition of a N-Tone Table
            For Definition on a N-Tone Table see Documentation 
            This Function is Public
        """
        # Construct Prototype Matrix
        temp_pro_table = self.construct_vector()
        for i in range(2, (self.dy)):
            temp_pro_table = np.vstack((temp_pro_table,
                                        self.construct_vector()))
        # Transpose Matrix
        temp_trans_pro_table = temp_pro_table.transpose()
        # Building Derivative Table
        temp_netto_deriv = (temp_trans_pro_table-int(100/self.dy))
        # Adding horizontal
        temp_netto_sum = np.sum(temp_netto_deriv, axis=1)
        # Vector * -1 => Solution Vector
        temp_netto_sum_inverse = array(temp_netto_sum * -1)
        # Building Summ of Vector
        temp_vector_sum = np.sum(temp_netto_sum_inverse+50)
        # Calculate Divisor
        temp_fact = 100/temp_vector_sum
        # Multiplic Vector
        temp_netto_sum_inverse_m = temp_netto_sum_inverse * temp_fact
        # Multiplic Matrix
        temp_netto_deriv_m = temp_netto_deriv * temp_fact
        # Integrate Matrix

        temp_pro_table = temp_netto_deriv_m + (100/self.dx)
        # Integrate Vector
        temp_ofset = temp_netto_sum_inverse_m + (100/self.dx)

        # Add Solution Vector to Transpose Matrix
        temp_table = np.c_[temp_pro_table, temp_ofset]
        # Write ready Matrix in Master-Table
        self.master_table = temp_table.astype(int)


class Tables:
    """ Class for Tables
    Contain:
        + Widets Construct
        + Reader / Writer Methods
        + Load and Save Methods
        + Special Handlers        """

    # Size of Base Table
    _height = 9  # heigth of Base Table
    _width = 9  # width of Base Table
    _used = 3 # Used Matrix Sice

    """ Method for Initalising and Widgest Construct
        This Method is private
    """
    def __init__(self, panel):  # Parameter: panel: Place to put Table
        self.cells = {}  # Create Array to hold Entrys on Table
        for i in range(self._height):  # Iterate over Rows
            for j in range(self._width):  # Iterate over Columns
                this_entry = Entry(panel,  # place Entry
                                   text="",  # Set Text to Empty
                                   width=3
                                   # Set Space for Values of Percet in Integer
                                   )  # Construct Entry
                this_entry.grid(row=i,
                                column=j)  # Send Entry to Grid Manager
                self.cells[i, j] = this_entry
                # Store Entry Widget in Tables Array
                
    def set_used(self, x):
        self._used = x

    def read_cell(self, rows, column):
        """ Method for Read Value from Table
                rows, column = Position from Table to read
            This Methode is Public
        """
        # Return Value for specifuc Position
        return self.cells[rows, column].get()

    def write_cell(self, rows, column, value):
        """ Method to Write to Table
            rows, column = Position from Table to write
            value        = String to write
            This Method is Public
        """
        self.cells[rows, column].delete(first=0, last="end")  # Emtying Entry
        self.cells[rows, column].insert(index=0, string=value)  # Write Value

    def interchange(self, to_copy_table):
        """ Method to change Values between Tables
                to_copy_table = other Table to interchange
            This Method is Public
        """
        for i in range(self._height):  # Iterrating over Rows
            for j in range(self._width):  # Iterating over Columns
                temp = self.read_cell(rows=i, column=j)
                self.write_cell(rows=i, column=j,
                                value=to_copy_table.read_cell(i, j))
                to_copy_table.write_cell(rows=i, column=j,
                                         value=temp)  # Process of Interchange

    """ Helper-Function to write whole Row to Table
            line = String of Line / Row to write
            i    = Number of Row to write
        This Function is strict Private
    """
    def _load_helper(self, line, i):
        j = 0  # intilaise Column Number

        temp = line.split()  # Split of line in Words corresponds with cells

        for word in temp:  # Iterate through Cells of Row
            self.write_cell(rows=i, column=j, value=word)  # write Value String
            j = j + 1

    def load_table(self):
        """ Method to load Table from File
            This Method is Public
        """
        i = 0  # initialise Row Number
        # Display Dialog
        test = filedialog.askopenfilename(title="Lade Werte aus Datei... ???",
                                          filetypes=(
                                              # Chose Default File-Type
                                              ("N-Tone Tables", "*.ntone"),
                                              # Choose all File Types
                                              ("all files", "*.*")
                                              ))

        if test != "":  # Look is Pfad to File consistent
            with open(file=test) as file:  # Open right File
                for line in file:  # Read whole Line from File
                    # Call Helper Function to write whole Row
                    self._load_helper(line=line.rstrip(),
                                      i=i)
                    i = i + 1

    """Method to save Table to File
        This Method is Public
    """
    def save_table(self):
        """Method to save Table to File
             This Method is Public
        """
        test = filedialog.asksaveasfile(title="Schreibe zu Datei ... ???",
                                        filetypes=(
                                            # Choose default File-Typ
                                            ("N-Tone Tables", "*.ntone"),
                                            # Choose all File Types
                                            ("all files", "*.*")
                                            ))  # File Chooser for Save-File
        if test != "":  # Look is Pfad to File consistent
            fobj = open(file=test.name, mode="w")  # Open right File

            for i in range(self._height):  # Iterate over Rows
                for j in range(self._width):  # Iterrrate over Columnns
                    # Read Value out of Cell
                    temp = self.read_cell(rows=i, column=j)
                    fobj.write(temp + " ")  # Write Value to File
                fobj.write("\n")  # Cloose Line of File

        fobj.close()  # Closse File

    def build_derivative(self, probab_table):
        """ Method to build Derivative Tables
            probab_table = Table from where to build Derivate
            This Method is Public
        """
        for i in range(self._height):  # Iteratte over Rows
            for j in range(self._width):  # Iteratte over Columns
                # read from Probablistic Table
                temp = probab_table.read_cell(rows=i, column=j)

                try:  # try to translate to int
                    temp = str(int(temp)-(100//self._used ))
                except ValueError:  # if this failure Set Value to ""
                    temp = ""
                self.write_cell(rows=i, column=j,
                                # Write Derivative Value back to Derivative Tab
                                value=temp)

    def erase_table(self):
        """ Method to build Derivative Tables
            probab_table = Table from where to build Derivate
            This Method is Public
        """
        for i in range(self._height):  # Iteratte over Rows
            for j in range(self._width):  # Iteratte over Columns
                # read from Probablistic Table
                temp = ""
                self.write_cell(rows=i, column=j,
                                # Write Derivative Value back to Derivative Tab
                                value=temp)


def generate_table(tableprob, tableder):
    """ Wraper Function for N-Tone Table Generation
        tableprob = Probablistic Table to save Values
        tableder  = Corresponding Derrivative Table
        This Function is Private
    """
    tempa = CalcTabel() # Generates Empty Table
    # Ask for Size N of the N*N Matrix to create
    num = askinteger('Generate Table', 'Please Enter Size of Table')
    if num: # Checks is there a correct N Size for the Table
        tempa.set_delta(num) # Set the Size of the N-Tone Table
        tempa.construct_table() # Construct the N-Tone Table
        tableder.set_used(num) # Set Correct Size of the Derivative Table
        tableprob.erase_table() # Erase all Values of the Probablistic Table
        # Write Values from the N-Tone Table into the Probablistic Table 
        # Widget
        tempa.transpond(tableprob)
        # Create Corresponding Derivative Table
        tableder.build_derivative(probab_table=tableprob)


def hello_call_back():
    """ Dummy-Method for Menue und Butto Commands
        In Because of Debuging this Function prints simple Test to Console
        This is a poore Debugging Method
    """
    print("Test")


def load_table(table1, table2):
    """ Wraper Function for Load-Table Command
        table1 = Selected Probablistic Table
        table2 = Selected Derivative Table
        This Method is Public
    """
    table1.load_table()
    table2.build_derivative(probab_table=table1)


def about_info():
    """ Wraper Function for About-Info Dialog
            This Method is Public
    """
    messagebox.showinfo("About the N-Tone Serialism Tollkit",
                        "I'm happy to see you will give it a Chance\n" +
                        "This is my little N-Tone Serialism Tollkit\n" +
                        "My Name is Thomas Lahme and I working for the " +
                        "CreCo Projec\n" +
                        "And for more Informations see my Projet at " +
                        "IRCAM:\n" +
                        "https://git.forum.ircam.fr/creco/make-a-melody/" +
                        "-/wikis/Serialism-Toll-Kit" +
                        "\n\n This Programm is licensed ounder the GPL 3" +
                        " or later")


def open_documentation():
    """ Wraper Function open Documentation in Default Browser
        This Methode is Public
    """
    # URL-Address of Documentation
    url = "https://git.forum.ircam.fr/creco/make-a-melody/-/wikis/Serialism-Toll-Kit"
    # Open this URL
    web.open(url)


class Example(Frame):
    """ Construct of the Main-Window
    """

    """ Construct Tables for Derivative-Tables
        This Method is Private
    """
    def _creating_derivativ_tables(self):

        """ Construct Tables for Derivative-Tables
            This Method is Private
        """
        self.pbt2 = PanedWindow(master=self.window_main_panel)

        # Construct Panels for Single Tabels A B Res
        pbta = PanedWindow(master=self.pbt2)
        pbtb = PanedWindow(master=self.pbt2)
        pbtres = PanedWindow(master=self.pbt2)

        # Construct Tables in this Panels
        self.derivat_table_a = Tables(panel=pbta)
        self.derivat_table_b = Tables(panel=pbtb)
        self.derivat_table_res = Tables(panel=pbtres)

        # Add Panels for A B Res to main Panel
        self.pbt2.add(pbta)
        self.pbt2.add(pbtb)
        self.pbt2.add(pbtres)

        # Add main Panel to main Panel from main Window
        self.window_main_panel.add(self.pbt2)

    def _creatingprobab_tables(self):
        """ Construct Tables for Probablistic Tables
            This Method is Private
        """
        self.pbt1 = PanedWindow(master=self.window_main_panel)

        # Construct Panels for Single Tabels A B Res
        pbta = PanedWindow(master=self.pbt1)
        pbtb = PanedWindow(master=self.pbt1)
        pbtres = PanedWindow(master=self.pbt1)

        # Construct Tables in this Panels
        self.probab_table_a = Tables(panel=pbta)
        self.probab_table_b = Tables(panel=pbtb)
        self.probab_table_res = Tables(panel=pbtres)

        # Add Panels for A B Res to main Panel
        self.pbt1.add(pbta)
        self.pbt1.add(pbtb)
        self.pbt1.add(pbtres)

        # Add main Panel to main Panel from main Window
        self.window_main_panel.add(self.pbt1)

    def do_interchange(self, mode):
        """ Wraper Function for Commandos Do-Interchange
                mode = Selection which Tables to Interchange
            This Method is Public
        """
        if mode == "AB":
            self.probab_table_a.interchange(to_copy_table=self.probab_table_b)
            self.derivat_table_a.interchange(to_copy_table=self.derivat_table_b)

        # Interchange Tables A with Res
        if mode == "ARes":
            self.probab_table_a.interchange(to_copy_table=self.probab_table_res)
            self.derivat_table_a.interchange(to_copy_table=self.derivat_table_res)

        # Interchange Tables B with Res
        if mode == "BRes":
            self.probab_table_b.interchange(to_copy_table=self.probab_table_res)
            self.derivat_table_b.interchange(to_copy_table=self.derivat_table_res)

    def save_table(self):
        """ Wrapper Save Probablistic Table Res to File
        """
        self.probab_table_res.save_table()

    def _creating_commands(self):
        """ Construct of Button Panel
            This Method is Private
        """
        # Creating Main Panel for Buttons
        m_command = PanedWindow(master=self.window_main_panel, orient=VERTICAL)
        wide = 22  # Widde of Buttons for Formating

        # Buttons
        # Add Buttons to main Panel
        command1 = Button(master=m_command,
                          text="Generate New Score A",
                          command=lambda: generate_table(tableprob=self.probab_table_a, tableder=self.derivat_table_a),
                          width=wide)
        command1.grid(row=0, column=0)

        command2 = Button(master=m_command,
                          text="Generate New Score B",
                          command=lambda: generate_table(tableprob=self.probab_table_b, tableder=self.derivat_table_b),
                          width=wide)
        command2.grid(row=0, column=1)

        command3 = Button(master=m_command,
                          text="Coomand-3",
                          command=hello_call_back,
                          width=wide)
        command3.grid(row=0, column=2)

        command4 = Button(master=m_command,
                          text="Coomand-4",
                          command=hello_call_back,
                          width=wide)
        command4.grid(row=0, column=3)

        command5 = Button(master=m_command,
                          text="Coomand-5",
                          command=hello_call_back,
                          width=wide)
        command5.grid(row=1, column=0)

        command6 = Button(master=m_command,
                          text="Coomand-6",
                          command=hello_call_back,
                          width=wide)
        command6.grid(row=1, column=1)

        command7 = Button(master=m_command,
                          text="Coomand-7",
                          command=hello_call_back,
                          width=wide)
        command7.grid(row=1, column=2)

        command8 = Button(master=m_command,
                          text="Coomand-8",
                          command=hello_call_back,
                          width=wide)
        command8.grid(row=1, column=3)

        # Add Buttons main Panel to Window Main Panel
        self.window_main_panel.add(m_command)

    def _creating_menu(self):
        """ Construct Menu
            This Methode is Private
        """
        # Build up and configure Main Menue
        menubar = Menu(master=self.master)
        self.master.config(menu=menubar)

        # Build first level Under Menue
        load_save_menu = Menu(master=menubar)  # Menue for Load Save Operations
        move_table_menu = Menu(master=menubar)  # Menue for Table Interchanges
        help_menu = Menu(master=menubar)  # Menue for Help and Documentation

        # Add first level Menues to main Menue
        menubar.add_cascade(label="Load / Save",  # Title of Load Save Menue
                            menu=load_save_menu)
        menubar.add_cascade(label="Move Tables",  # Title of Interchange Menue
                            menu=move_table_menu)
        menubar.add_cascade(label="Help",  # Title of Help Menue
                            menu=help_menu)

        load_save_menu.add_command(label="Load to Table A",
                                   command=lambda: load_table(table1=self.probab_table_a, table2=self.derivat_table_a)
                                   )  # Menue Entry for Load File Content in Table A
        load_save_menu.add_command(label="Load to Table B",
                                   command=lambda: load_table(table1=self.probab_table_b, table2=self.derivat_table_b)
                                   )  # Menue Entry for Load File Content in Table B
        load_save_menu.add_command(label="Save Table Res...",
                                   command=self.save_table
                                   )  # Menue Entry for Save Res Table into File
        move_table_menu.add_command(label="Move Table Res to A",
                                    command=lambda: self.do_interchange(mode="ARes")
                                    )  # Menue Entry for Interchange Tables A with Res
        move_table_menu.add_command(label="Move Table Res to B",
                                    command=lambda: self.do_interchange(mode="BRes")
                                    )  # Menue Entry for Interchange Tables B with Res
        move_table_menu.add_command(label="Interchange Table A with B",
                                    command=lambda: self.do_interchange(mode="AB")
                                    )  # Menue Entry for Interchange Tables A with B

        help_menu.add_command(label="About the N-Tone Serialism Tollkit",
                              command=about_info
                              )  # Menue Entry for Command Show About-Info
        help_menu.add_command(label="open_documentation ... ",
                              command=open_documentation
                              )  # Menue Entry for Command open Documentation

    def __init__(self):
        """ TKInter Main Window Initialising
            This is the Main Function and Private
        """

        super().__init__()

        self.initUI()

    def initUI(self):
        """Construct Main Window
            This is the Public Main Function
        """

        # Set Title of Main Window
        self.master.title("N-Tone Tollkit")

        # Creating Menues call Construct Method for Menue
        self._creating_menu()

        # Creating Main-Window Main-Panel
        self.window_main_panel = PanedWindow(orient=VERTICAL)

        # Creating Main-Window Content
        self._creatingprobab_tables()  # Creating Probalistic Tables by call Method Construct
        self._creating_derivativ_tables()  # Creating Derivative Tables by call Method Construct
        self._creating_commands()  # Creating Button Grid by call Method Construct

        # Pack Content to Main-Window and Expand to it
        self.window_main_panel.pack(fill=BOTH, expand=1)

        self.pack(fill=BOTH, expand=1)


def main():
    """ Construct Application
    """

    root = Tk()
    root.geometry("750x400+300+300")
    app = Example()
    root.mainloop()


if __name__ == '__main__':
    main()
